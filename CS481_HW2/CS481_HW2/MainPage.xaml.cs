﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2
{
	public partial class MainPage : ContentPage
	{
        //These are the hidden and shown values the calculator tracks
        static int stored = 0;
        static int displayed = 0;
        //This is for keeping track of the last operand symbol entered.
        static int mode = 0;

        public MainPage()
		{
			InitializeComponent();
		}
        public void SolveCall()
        {
            //Result is implemented in order to fix some bugs that were happening
            //when the value for displayed was modified directly.
            int result = 0;
            switch (mode)
            {
                case 1: //Addition
                    result = stored + displayed;
                    break;
                case 2: //Subtraction
                    result = stored - displayed;
                    break;
                case 3: //Multiplication
                    result = stored * displayed;
                    break;
                case 4: //Division
                    result = stored / displayed;
                    break;
            }
            mode = 0;
            this.DispValue.Text = result.ToString();
            stored = result;
            displayed = 0;
            //This does not show the user a 0, but allows for the calculator
            //to handle chain calculations (with no priority)
        }
        private void EqCall(object sender, EventArgs e)
        {
            //Originally tried to have the equals button call SolveCall direct
            //But there were some issues, and as such this wrapper was implemented.
            SolveCall();
        }
        private void FuncCall(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (mode != 0)
            {
                //Checks for if there were already two numbers in storage
                //And if so, solves them before continuing.
                SolveCall();
            }
            else
            {
                //Moves the most recently entered number into the hidden storage.
                stored = displayed;
                displayed = 0;
            }
            switch (b.Text)
            {
                case "+": mode = 1;
                    break;
                case "-": mode = 2;
                    break;
                case "*": mode = 3;
                    break;
                case "/": mode = 4;
                    break;
            }
        }
        private void NumberCall(object sender, EventArgs e)
        {
            //Source for button idea: https://forums.xamarin.com/discussion/44152/get-id-button-in-a-click-event
            Button but = (Button)sender;
            //Shift the value one digit to the left
            displayed = displayed * 10;
            int n = Convert.ToInt32(but.Text);
            displayed += n;
            this.DispValue.Text = displayed.ToString();
        }
        private void ClearCall(object sender, EventArgs e)
        {
            //Clears all entered data
            displayed = 0;
            stored = 0;
            mode = 0;
            this.DispValue.Text = displayed.ToString();
        }
    }
}
